﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using TermExport.Data;

using Terms = TermExport.Models.Terms;

namespace TermExport.Controllers
{
    public class TermController : Controller
    {
        // GET: Term
        public ActionResult Term()
        {
            var model = new List<Terms>();            
            
            return View(model);
        }

        public FileStreamResult GenerateXML()
        {
            MemoryStream ms = new MemoryStream();
            XmlWriterSettings xws = new XmlWriterSettings();
            xws.OmitXmlDeclaration = true;
            xws.Indent = true;

            using (XmlWriter xw = XmlWriter.Create(ms, xws))
            {
                XDocument doc = new XDocument(
                    new XElement("Tidrans",
                        new XElement("tidkod", "role"),
                        new XElement("datum", "date"),
                        new XElement("timmar", "hours")
                    )
                );
                doc.WriteTo(xw);
            }
            ms.Position = 0;
            return File(ms, "text/xml", "Sample.xml");
        }
    

    //public void GenerateXml()
    //{
    //var termList = _termer
    //var count = 1;
    //var doc = new XElement("terms");
    //    foreach (var term in termList)
    //{
    //    doc.Add(new XElement("term",
    //            new XElement("SLNO", count),
    //            new XElement("NAME", item.Employee_Name),
    //            new XElement("DESIGNATION", item.Employee_Designation),
    //            new XElement("EMAIL", item.Employee_Email),
    //            new XElement("ADDRESS", item.Employee_Address)
    //        )
    //    );
    //    count += 1;
    //}
    //string fileName = "termexport" +
    //                  string.Format("{0:yyyy_MM_dd}", DateTime.Now) + ".xml";
    ////Response.ContentType = "text/xml";
    ////Response.AddHeader("content-disposition", "attachment; 

    ////filename =\"" + fileName + "\"");
    ////Response.Write(doc);
    ////Response.End();
    //}
}
}