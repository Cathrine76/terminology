﻿using System;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Mvc;
using System.Web.Razor.Parser;
using System.Xml;
using System.Xml.Linq;
using Antlr.Runtime.Misc;
using Microsoft.Ajax.Utilities;
using TermExport.Data;
using TermExport.Models;
using WebGrease.Css.Ast.Selectors;

namespace TermExport.Controllers
{
    public class HomeController : Controller
    {
        private static readonly TermData _terms = new TermData();     

        private List<Source> GetSource(List<TermData> termList)
        {
            var sourceList = _terms.GetSourceList();
            List<Source> _list = new List<Source>();
            //sourceList.RemoveAll(a => !termList.Exists(b => a.TermTextId == b.TermTextId));
            foreach (var source in sourceList)
            {
                _list.Add(new Source()
                {
                    TermEntryId = source.TermEntryId,
                    TermId = source.TermTextId,                    
                    TermSource = source.TermSource
                });
            }


            return _list;
        }

        private List<IcsCode> GetIcs(List<TermData> termList)
        {
            List<IcsCode> _list = new List<IcsCode>();
            var ics = _terms.GetIcsCodes();


            return _list;
        }



        public ActionResult Index()
        {


            GenerateXml();


                return View();
        }


        public void GenerateXml()
        {
            var result = _terms.GetTermList();
            var termDoc = Createtermxml(result);
         

           
        }

        private static string GetDefKey(int termEntryId, string language) => $"{termEntryId}, {language}";
                   
        

        private static XDocument Createtermxml(List<TermData> result)
        {
            

            XDocument xmlDoc = new XDocument();
           
            //var icsCodes = _terms.GetIcsCodes();

            var definitions = Definitionsgrouped(result);
            var notes = NotesGrouped(result);
            var synonymes = SynonymesGrouped(result);
            var formulas = FormulasGrouped(result);
            var sources = SourceGrouped();
            var icsCodes = IcsCodesGrouped();
            var terms = (from r in result
                where r.TypeName == "Hovedterm"
                select new
                {
                    r.TermEntryId,
                    r.MainTermText,
                    r.Language
                });

            var xmlTerm =
                (from term in terms
                group term by term.TermEntryId
                into grp
                let termid = grp.Key
                select new
                {
                    termid,
                    Ics = icsCodes.ContainsKey(termid) ? icsCodes[termid] : new List<IcsCode>(),
                    terms = from g in grp
                    group g by g.Language
                        into grplang
                            let lang = grplang.Key
                            let termdata = grplang.First()
                            let p = GetDefKey(termid, lang)
                            select new
                            {
                                lang,                                
                                Termdata = (from t in grplang
                                           group t by t.TermEntryId
                                into tgroup
                                select new
                                {
                                    tgroup.Key,
                                    term = tgroup,
                                    Def = definitions.ContainsKey(p) ? definitions[p] : new List<string>(),
                                    Note = notes.ContainsKey(p) ? notes[p] : new List<string>(),
                                    Synonyme = synonymes.ContainsKey(p) ? synonymes[p] : new List<string>(),
                                    Formula = formulas.ContainsKey(p) ? formulas[p] : new List<string>(),
                                    Source = sources.ContainsKey(p) ? sources[p] : new List<string>()                                    
                                })
                            }

                });


                var xterms = new XElement("terms",
                    from termentry in xmlTerm
                    select new XElement("termentry",
                        new XAttribute("termentryid", termentry.termid),
                        from lang in termentry.terms
                        from item in lang.Termdata
                        select new XElement("term",
                            new XAttribute("language", lang.lang),
                            from t in item.term
                            select new XElement("mainterm", RemoveInvalidXmlChars(t.MainTermText)),
                            new XElement("definitions",
                            from def in item.Def
                            select new XElement("definition", RemoveInvalidXmlChars(def))),
                            new XElement("notes",
                            from note in item.Note
                            select new XElement("note", RemoveInvalidXmlChars(note))),
                            new XElement("synonymes",
                            from synonyme in item.Synonyme
                            select new XElement("synonyme", RemoveInvalidXmlChars(synonyme))),
                            new XElement("formulas",
                                from formula in item.Formula
                                select new XElement("formula", RemoveInvalidXmlChars(formula))),
                                new XElement("sources",
                                    from source in item.Source
                                    select new XElement("source", RemoveInvalidXmlChars(source)))),
                            new XElement("icscodes",
                            from icscode in termentry.Ics                           
                            select new XElement("icstitle", RemoveInvalidXmlChars(icscode.IcsTitle),
                            new XAttribute("icscode", RemoveInvalidXmlChars(icscode.Ics))

                    )))
                    );

           xmlDoc.Add(xterms);
          
            xmlDoc.Save(@"C:\Users\by20\Documents\terms.xml");
            return xmlDoc;

        }



        private static Dictionary<string, List<string>> Definitionsgrouped(List<TermData> result)
        {
            var definitions = (from r in result
                where r.TypeName == "Definisjon"
                select (new
                {
                    r.TermEntryId,
                    r.TermId,
                    r.TermTextId,
                    r.TermText,
                    r.Language
                }));
           
                var definitionsgrouped = (from def in definitions
                    group def by new { def.Language, def.TermEntryId }
                    into defgroup
                    select new
                    {
                        Key = defgroup.Key,
                        Def = defgroup
                    }).ToDictionary(d => GetDefKey(d.Key.TermEntryId, d.Key.Language), d => d.Def.Select(t => t.TermText).ToList());


            return definitionsgrouped;
        }

        private static Dictionary<string, List<string>> NotesGrouped(List<TermData> result)
        {
            var notes = (from r in result
                where r.TypeName == "Merknad"
                select (new
                {
                    r.TermEntryId,
                    NoteText = r.TermText,
                    r.Language
                }));

            var notesgrouped = (from note in notes
                group note by new { note.Language, note.TermEntryId }
                into notegroup
                select new
                {
                    Key = notegroup.Key,
                    Note = notegroup
                }).ToDictionary(d => GetDefKey(d.Key.TermEntryId, d.Key.Language), d => d.Note.Select(t => t.NoteText).ToList());


            return notesgrouped;
        }

        private static Dictionary<string, List<string>> SynonymesGrouped(List<TermData> result)
        {
            var synonymes = (from r in result
                where r.TypeName == "Synonym"
                select (new
                {
                    r.TermEntryId,
                    SynonymeText = r.TermText,
                    r.Language
                }));

            var synonymesgrouped = (from synonyme in synonymes
                group synonyme by new { synonyme.Language, synonyme.TermEntryId }
                into syngroup
                select new
                {
                    Key = syngroup.Key,
                    Syn = syngroup
                }).ToDictionary(d => GetDefKey(d.Key.TermEntryId, d.Key.Language), d => d.Syn.Select(t => t.SynonymeText).ToList());
            return synonymesgrouped;
        }

        private static Dictionary<string, List<string>> FormulasGrouped(List<TermData> result)
        {
            var formulas = (from r in result
                where r.TypeName == "Formel"
                select (new
                {
                    r.TermEntryId,
                    FormulaText = r.TermText,
                    r.Language
                }));

            var formulasgrouped = (from formula in formulas
                                    group formula by new { formula.Language, formula.TermEntryId }
                into formgroup
                select new
                {
                    Key = formgroup.Key,
                    Form = formgroup
                }).ToDictionary(d => GetDefKey(d.Key.TermEntryId, d.Key.Language), d => d.Form.Select(t => t.FormulaText).ToList());
            return formulasgrouped;
        }

        private static Dictionary<string, List<string>> SourceGrouped()
        {
            var result = _terms.GetSourceList();
            var sources = (from r in result
                select (new
                {
                    r.TermEntryId,
                    SourceName = r.TermSource,
                    r.Language
                }));

            var sourcesgrouped = (from source in sources
                                    group source by new { source.Language, source.TermEntryId }
                into sourceroup
                                  select new
                {
                    Key = sourceroup.Key,
                    Source = sourceroup
                 }).ToDictionary(d => GetDefKey(d.Key.TermEntryId, d.Key.Language), d => d.Source.Select(t => t.SourceName).ToList());
            return sourcesgrouped;
        }

        private static Dictionary<int, List<IcsCode>> IcsCodesGrouped()
        {
            var result = _terms.GetIcsCodes();
            int count = result.Count();
            var icscodes = (from r in result
                select (new IcsCode()
                {
                    TermEntryId =  r.TermEntryId,
                    Ics = r.Ics,
                    IcsTitle = r.IcsTitle                    
                }));
                
                var icscodesgrouped = (from ics in icscodes
                                  group ics by  ics.TermEntryId 
                into icscodesgroup
                select new
                {
                    Key = icscodesgroup.Key,
                    ics = icscodesgroup
                }).ToDictionary(d => d.Key, d => d.ics.Select(t => t).ToList());
            return icscodesgrouped;
        }





        private static string RemoveInvalidXmlChars(string text)
        {
            if(!string.IsNullOrEmpty(text))
            { 
                var validXmlChars = text.Where(ch => XmlConvert.IsXmlChar(ch)).ToArray();
                return new string(validXmlChars);
            }

            return null;
        }

    }
}