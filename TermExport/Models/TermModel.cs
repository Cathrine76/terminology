﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TermExport.Models
{    

    public class Terms
    {
        [DisplayName("termentryid")]
        public int TermEntryId { get; set; }
        public List<Term> TermList { get; set; }
           
    }

    public class Term
    {
        public int TermEntryId { get; set; }
        [DisplayName("mainterm")]
        public string MainTerm { get; set; }
        [DisplayName("termid")]
        public int TermId { get; set; }
        [DisplayName("language")]
        public string Language { get; set; }
        public List<Definition> Definitions { get; set; }
        public List<Note> Notes { get; set; }
        public List<Source> Sources { get; set; }
        public List<IcsCode> IcsCodes { get; set; }

    }

    public class Definition
    {
        public int TermEntryId { get; set; }
        public int TermId { get; set; }
        [DisplayName("definition")]
        public string DefinitionText { get; set; }

    }

    public class Synonym
    {
        public int TermEntryId { get; set; }
        public int TermId { get; set; }
        [DisplayName("synonym")]
        public string Text { get; set; }

    }

    public class Note
    {
        public int TermEntryId { get; set; }
        public int TermId { get; set; }
        [DisplayName("notes")]
        public string NoteText { get; set; }
    }

    public class Source
    {
        public int TermEntryId { get; set; }
        public int TermId { get; set; }
        [DisplayName("Termkilde")]
        public string TermSource { get; set; }

    }

    public class IcsCode
    {
        public int TermEntryId { get; set; }
        public string Language { get; set; }
        [DisplayName("ICS koder")]
        public string Ics { get; set; }
        public string IcsTitle { get; set; }

    }

}

