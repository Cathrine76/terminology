﻿using System.Configuration;
using TermExport.DataContext;
using System.Data.Linq;
using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;


namespace TermExport.Data
{
    public class TermData
    {
        #region private
        private string ConnStr = null;
        #endregion
     
        #region constructor

        public TermData()
        {
            ConnStr = ConfigurationManager.ConnectionStrings["TermbasenSNORRE"].ConnectionString;
        }
        #endregion
        #region public members

        #region properties      
        public int TermEntryId { get; set; }
        public int TermId { get; set; }
        public int TermTextId { get; set; }
        public string MainTermText { get; set; }
        public string TermText { get; set; }
        public string NoteText { get; set; }
        public string SynonymeText { get; set; }
        public string TypeName { get; set; }
        public string TermSource { get; set; }
        public string Language { get; set; }
        public string Ics { get; set; }
        public string IcsTitle { get; set; }
        public string Subject { get; set; }
        #endregion
        #endregion
        #region private

        Func<DC_TermEntryDataContext, IQueryable<TermData>> CompiledGetTermList =>
            CompiledQuery.Compile((DC_TermEntryDataContext dbContext) =>
                (from termentries in dbContext.vw_TermListExports
                    where termentries.PublishStatus == 1 && (termentries.FASTLanguageCode == "nn" || termentries.FASTLanguageCode == "nb" || termentries.FASTLanguageCode == "en")
                 orderby termentries.TermEntryId
                    select new TermData()
                    {
                        TermEntryId = termentries.TermEntryId,
                        TermId = termentries.TermId,
                        TermTextId = termentries.TextId,
                        Language = termentries.FASTLanguageCode,
                        TypeName = termentries.TypeName,
                        MainTermText = termentries.MainTermText,
                        TermText = termentries.Text
                                  
                    }));

        Func<DC_TermEntryDataContext,  IQueryable<TermData>> CompiledGetSourceTerm =>
            CompiledQuery.Compile((DC_TermEntryDataContext dbContext) =>
                (from source in dbContext.vw_TermSourceExports
                    orderby source.TermEntryId
                    select new TermData()
                    {
                        TermEntryId = source.TermEntryId,
                        TermText = source.SourceName,
                        Language = source.FASTLanguageCode

                    }));
 
        Func<DC_TermEntryDataContext,  IQueryable<TermData>> CompiledGetIcsCodes =>
            CompiledQuery.Compile((DC_TermEntryDataContext dbContext) =>
                (from ics in dbContext.vw_IcsExports
                    orderby ics.TermEntryId
                    select new TermData()
                    {
                        TermEntryId = ics.TermEntryId,
                        Ics = ics.ICSCode,
                        IcsTitle = ics.ICSCodeTitle
                        
                    }));


        private List<TermData> GetTerms()
        {
            using (SqlConnection conn = new SqlConnection(ConnStr))
            {
                using (DC_TermEntryDataContext dbContext = new DC_TermEntryDataContext(ConnStr))
                {
                    return CompiledGetTermList(dbContext).ToList();
                }
            }
        }

        private List<TermData> GetSource()
        {
            using (SqlConnection conn = new SqlConnection(ConnStr))
            {
                using (DC_TermEntryDataContext dbContext = new DC_TermEntryDataContext(ConnStr))
                {
                      return CompiledGetSourceTerm(dbContext).ToList();                   
                }
                   
            }
        }

        private List<TermData> GetIcsCode()
        {
            using (SqlConnection conn = new SqlConnection(ConnStr))
            {
                using (DC_TermEntryDataContext dbContext = new DC_TermEntryDataContext(ConnStr))
                {
                    return CompiledGetIcsCodes(dbContext).ToList();
                }

            }
        }

        #endregion

        #region public

        public List<TermData> GetTermList()
        {            
            var terms = GetTerms();
            
            
            return terms;
        }

        public List<TermData> GetSourceList()
        {

            var data = new List<TermData>();
            var sources = GetSource().ToList();
            foreach (var source in sources)
            {
                data.Add(new TermData()
                {
                    TermEntryId = source.TermEntryId,
                    TermSource = source.TermText,
                    Language = source.Language
                });
            }
            

            return data;
        }

        public List<TermData> GetIcsCodes()
        {

            var data = new List<TermData>();
            var sources = GetIcsCode().ToList();
            foreach (var source in sources)
            {
                data.Add(new TermData()
                {
                    TermEntryId = source.TermEntryId,
                    Ics = source.Ics,
                    IcsTitle = source.IcsTitle
                });
            }


            return data;
        }
        #endregion
    }

}