﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;


namespace TermImport
{
    class TermImport
    {
     

        static void Main(string[] args)
        {
            Console.WriteLine("Starter importen");
           
           GetTermer();
        }


        //Hente ut aktuelle standarder
        public static void GetTermer()
        {
            try
            {
                OleDbConnection Conn = new OleDbConnection();
                OleDbDataAdapter terms = new OleDbDataAdapter();
                System.Data.DataSet termtable = new DataSet();
                Conn.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data source=C:\Utvikling\TermImport\Access\NewTerms.accdb";
                Conn.Open();
                string sql;
                sql = "SELECT termer.[term-en], termer.[syno-en], termer.[def-en], termer.[note-en], termer.[term-nb], termer.[syno-nb], termer.[def-nb], termer.[note-nb], termer.[term-nn], termer.[syno-nn], termer.[def-nn], termer.[note-nn] FROM termer";

                terms.SelectCommand = new OleDbCommand(sql, Conn);
                Conn.Close();
                terms.Fill(termtable);
                foreach (DataTable _terms in termtable.Tables)
                {
                    foreach (DataRow termdef in _terms.Rows)
                    {
                        CreateTermEntry();
                        if (!string.IsNullOrEmpty(termdef["term-en"].ToString()))
                        { 
                            InsertTerms(termdef["term-en"].ToString(), 4, 3);
                            Console.WriteLine("Term en: " + termdef["term-en"].ToString());
                        }
                        if (!string.IsNullOrEmpty(termdef["syno-en"].ToString()))
                        { 
                            InsertTerms(termdef["syno-en"].ToString(), 4, 5);
                            Console.WriteLine("Synonym en: " + termdef["syno-en"].ToString());
                        }
                        if (!string.IsNullOrEmpty(termdef["def-en"].ToString()))
                        {
                            InsertDefinitions(termdef["def-en"].ToString(), 4, 1);
                            Console.WriteLine("Definisjon en: " + termdef["def-en"].ToString());
                        }
                        if (!string.IsNullOrEmpty(termdef["note-en"].ToString()))
                        {
                            InsertDefinitions(termdef["note-en"].ToString(), 4, 2);
                            Console.WriteLine("note en: " + termdef["note-en"].ToString());
                        }

                        //bokmål
                        if (!string.IsNullOrEmpty(termdef["term-nb"].ToString()))
                        {
                            InsertTerms(termdef["term-nb"].ToString(), 2, 3);
                            Console.WriteLine("Term nb: " + termdef["term-nb"].ToString());
                        }
                        if (!string.IsNullOrEmpty(termdef["syno-nb"].ToString()))
                        {
                            InsertTerms(termdef["syno-nb"].ToString(), 2, 5);
                            Console.WriteLine("syno nb: " + termdef["syno-nb"].ToString());
                        }
                        if (!string.IsNullOrEmpty(termdef["def-nb"].ToString()))
                        {
                            InsertDefinitions(termdef["def-nb"].ToString(), 2, 1);
                            Console.WriteLine("def nb: " + termdef["def-nb"].ToString());
                        }
                        if (!string.IsNullOrEmpty(termdef["note-nb"].ToString()))
                        {
                            InsertDefinitions(termdef["note-nb"].ToString(), 2, 2);
                            Console.WriteLine("note nb: " + termdef["note-nb"].ToString());
                        }

                        //Nynorsk                        
                        if (!string.IsNullOrEmpty(termdef["term-nn"].ToString()))
                        {
                            InsertTerms(termdef["term-nn"].ToString(), 3, 3);
                            Console.WriteLine("Term nn: " + termdef["term-nn"].ToString());
                        }
                        if (!string.IsNullOrEmpty(termdef["syno-nn"].ToString()))
                        {
                            InsertTerms(termdef["syno-nn"].ToString(), 3, 5);
                            Console.WriteLine("syno nn: " + termdef["syno-nn"].ToString());
                        }
                        if (!string.IsNullOrEmpty(termdef["def-nn"].ToString()))
                        {
                            InsertDefinitions(termdef["def-nn"].ToString(), 3, 1);
                            Console.WriteLine("def nn: " + termdef["def-nn"].ToString());
                        }
                        if (!string.IsNullOrEmpty(termdef["note-nn"].ToString()))
                        {
                            InsertDefinitions(termdef["note-nn"].ToString(), 3, 2);
                            Console.WriteLine("note nn: " + termdef["note-nn"].ToString());
                        }
                    
                }
                }
                
                Conn.Close();
               
                
            }
            catch (Exception e)
            {                
                
            }
        }


        private static void InsertTerms(string text, int languageid, int termtypeid)
        {
            try
            {
                SqlConnectionStringBuilder builder =
                new SqlConnectionStringBuilder(GetConnectionString());
                builder.ConnectionString = "server=BYSVXD4E073;user id=eblutvikler;" +
          "password= eblutvikling;initial catalog=TermbasenSNORRE_drift";
               

                using (SqlConnection conn = new SqlConnection(builder.ConnectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spImportTerms";
                    cmd.Parameters.Add(new SqlParameter("@text", text));
                    cmd.Parameters.Add(new SqlParameter("@languageid", languageid));
                    cmd.Parameters.Add(new SqlParameter("@termTypeId", termtypeid));                    
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
                
            }
            catch (Exception ex)
            {
               
            }
        }

        private static void InsertDefinitions(string text, int languageid, int termtypeid)
        {
            try
            {
                SqlConnectionStringBuilder builder =
                new SqlConnectionStringBuilder(GetConnectionString());
                builder.ConnectionString = "server=BYSVXD4E073;user id=eblutvikler;" +
          "password= eblutvikling;initial catalog=TermbasenSNORRE_drift";


                using (SqlConnection conn = new SqlConnection(builder.ConnectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spImportDefinitions";
                    cmd.Parameters.Add(new SqlParameter("@text", text));
                    cmd.Parameters.Add(new SqlParameter("@languageid", languageid));
                    cmd.Parameters.Add(new SqlParameter("@definitionTypeId", termtypeid));
                    
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }

            }
            catch (Exception ex)
            {

            }
        }

        private static void CreateTermEntry()
        {
            int TermEntryID = int.MinValue;
            try
            {
                SqlConnectionStringBuilder builder =
                new SqlConnectionStringBuilder(GetConnectionString());
                builder.ConnectionString = "server=BYSVXD4E073;user id=eblutvikler;" +
          "password= eblutvikling;initial catalog=TermbasenSNORRE_drift";


                using (SqlConnection conn = new SqlConnection(builder.ConnectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand();                    
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "CreateTermEntry";
                    cmd.ExecuteNonQuery();
                    conn.Close();
                   
                }
               

            }
            catch (Exception ex)
            {
                
            }
        }



        private static string GetConnectionString()
        {
            // To avoid storing the connection string in your code,
            // you can retrieve it from a configuration file. 
            return "Server=(BYSVXD4E073);Integrated Security=false;" +
                "Initial Catalog=TermbasenSNORRE_drift";
        }


    }
}
